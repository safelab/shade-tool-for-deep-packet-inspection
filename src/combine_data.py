import os
import os.path
import argparse


def main():
    parser = argparse.ArgumentParser(description="Combine output files")
    parser.add_argument("data_directory", help="directory containing data files")
    parser.add_argument("output_file", help="output file name")
    

    args = parser.parse_args()

    data_files = [f for f in os.listdir(args.data_directory) if os.path.isfile(os.path.join(args.data_directory,f)) and "data" in f.split(".")[-1]]
        
    outFile = open(args.output_file, "w")

    isFirst = True

    for f in data_files:
        filePath = os.path.join(args.data_directory, f)
        fp = open(filePath, "r")
        print "Processing " + filePath + " ............"
        lines = fp.readlines()

        if isFirst:
            outFile.write("file,")
            outFile.write(lines[0])
            isFirst = False
        
        for i in range(1,len(lines)):
            outFile.write(f.split(".")[0]+",")
            outFile.write(lines[i])
    
        fp.close()

    outFile.close()

if __name__ == '__main__':
    main()
