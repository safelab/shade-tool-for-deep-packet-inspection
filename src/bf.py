import pybloomfilter

class bloomFilter():
    # Create a empty bloom filter
    def create_new_bf(self, capacity, error_rate, filename):
        self.bf = pybloomfilter.BloomFilter(capacity, error_rate, filename)

    # Open a existing bloom filter
    def open_bf(self, filename):
        self.bf = pybloomfilter.BloomFilter.open(filename)

    def add_item(self, item):
        self.bf.add(item)

    def check_membership(self, item):
        return item in self.bf

def main():
    # Create a empty bloom filter
    grams_bf = bloomFilter()
    grams_bf.create_new_bf(1000000, 0.01, "grams.bf")

    grams_bf.add_item("7C")
    grams_bf.add_item("7C0C")
    grams_bf.add_item("02")

    # Open a existing bloom filter
    grams_bf2 = bloomFilter()
    grams_bf2.open_bf("grams.bf")

    print grams_bf2.check_membership("7C")
    print grams_bf2.check_membership("7C0C")
    print grams_bf2.check_membership("02")
    print grams_bf2.check_membership("7C1C")

if __name__ == '__main__':
    main()