import os
import sys
import argparse
sys.path.append("../")
from bf import *

from extract_m221_wp import *
from os import listdir
from os.path import isfile


def main():
    parser = argparse.ArgumentParser(description="Generate n-gram bloom filters for M221 write packet payload")
    parser.add_argument("-l", "--lower-ngram", help="lower n-gram filter", required=True)
    parser.add_argument("-u", "--upper-ngram", help="upper n-gram filter", required=True)
    parser.add_argument("pcap_directory", help="directory containing pcap files")

    args = parser.parse_args()

    pcap_files = [f for f in listdir(args.pcap_directory) if isfile(os.path.join(args.pcap_directory,f)) and "pcap" in f.split(".")[-1]]

    ngram_bf_list = []

    lower_n = int(args.lower_ngram)
    upper_n = int(args.upper_ngram)

    # output directory
    bf_dir = "./bf/"

    if not os.path.exists(os.path.dirname(bf_dir)):
        try:
            os.makedirs(os.path.dirname(bf_dir))
        except:
            print "Error. failed to make dir: " + os.path.dirname(bf_dir)

    for i in range(lower_n, upper_n+1):
        ngram_bf_list.append(bloomFilter())
        ngram_bf_list[i-lower_n].create_new_bf(1000000, 0.01, bf_dir+str(i)+"gram_m221.bf")
        
    for f in pcap_files:
        filePath = os.path.join(args.pcap_directory,f)

        print "processing " + filePath + " ......."

        wp_list = extract_m221_wp(filePath)       
    
        for wp in wp_list:
            if wp.isCode == True:
                for i in range(len(wp.payload)):
                    for n in range(lower_n, upper_n+1):
                        if len(wp.payload) - i >= n:
                            ngram_bf_list[n-lower_n].add_item(wp.payload[i:i+n])
                            #print "add: " + binascii.hexlify(wp.payload[i:i+n])

if __name__ == '__main__':
    main()
