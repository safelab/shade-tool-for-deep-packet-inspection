# Shadow Memory Optimization 1. Update only continuous area (block)
from sortedcontainers import SortedList

from scapy.all import *
import sys, os
import argparse

# transaction id(2) + protocol identifier(2) + length(2) + unit identifier(1) (Not include function code byte)
MODBUS_HDR_LEN = 7

# DC_total: total number of bytes decompiled
# DC_longest: the longest bytes of decompiled
# RI_total: total number of rung identified
# OI_total: total number of opcodes identified
# OI_longest: the longest # of opcodes identified
# gX_total: total number of membership (x-gram)
# gX_longest: the longest # of membership (x-gram)

class Features():
    def __init__(self, DC_total, DC_longest, RI_total, OI_total, OI_longest, g1_total, g1_longest, g2_total, g2_longest, g3_total, g3_longest, g4_total, g4_longest, g5_total, g5_longest, g6_total, g6_longest):
        self.DC_total = DC_total
        self.DC_longest = DC_longest
        self.RI_total = RI_total
        self.OI_total = OI_total
        self.OI_longest = OI_longest
        self.g1_total = g1_total
        self.g1_longest = g1_longest
        self.g2_total = g2_total
        self.g2_longest = g2_longest
        self.g3_total = g3_total
        self.g3_longest = g3_longest
        self.g4_total = g4_total
        self.g4_longest = g4_longest
        self.g5_total = g5_total
        self.g5_longest = g5_longest
        self.g6_total = g6_total
        self.g6_longest = g6_longest


class M221_shadow_memory():
    def __init__(self):
        self.mem = bytearray('\x00') * 65536    # Memory space size = 0xffff (65536)
        # information of blocks: start_addr, end_addr, features
        # which is maintaiend by a sorted dictionary (key: addr)
        self.blocks = SortedList(key=lambda x: x[0])  
        self.mem_features = Features(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
          

    def process_pkts(self, pcapFile):
        all_pkts = rdpcap(pcapFile)
        modbus_pkts = (pkt for pkt in all_pkts if
            TCP in pkt and (pkt[TCP].dport == 502) and len(pkt[TCP].payload) > MODBUS_HDR_LEN)

        for pkt in modbus_pkts:
            modbus = bytes(pkt[TCP].payload)
            if modbus[MODBUS_HDR_LEN] != '\x5a':    # 0x5a is the function code indicating it is M221 message
                continue
            m221_msg = modbus[MODBUS_HDR_LEN+1:]

            # write request:
            if m221_msg[1] == '\x29':
                addr = struct.unpack("<H", m221_msg[2:4])[0]
                addr_type = m221_msg[4:6]
                size = struct.unpack("<H", m221_msg[6:8])[0]
                data = m221_msg[8:]   

                self.mem[addr:addr+size] = data
                self.process_new_payload(addr, size)

            # Scan shadow memory
            # self.scan_memory()
    """
    def scan_memory(self):
        # Apply various detection method here
        pass
    """

    def print_mem(self):
        f = open("shadow_mem", "w")
        f.write(self.mem)

    def scan(self, start_addr, end_addr):
        # get scan result for the selected range
        # below is dummy
        features = Features(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
        return features

    def update_shadow_memory_features(self):
        for b in self.blocks:
            self.mem_features.DC_total += b[2].DC_total
            self.mem_features.DC_longest = max(self.mem_features.DC_longest, b[2].DC_longest)
            self.mem_features.RI_total += b[2].RI_total
            self.mem_features.OI_total += b[2].OI_total
            self.mem_features.OI_longest = max(self.mem_features.OI_longest,b[2].OI_longest)
            self.mem_features.g1_total += b[2].g1_total
            self.mem_features.g1_longest = max(self.mem_features.g1_longest,b[2].g1_longest)
            self.mem_features.g2_total += b[2].g2_total
            self.mem_features.g2_longest = max(self.mem_features.g2_longest,b[2].g2_longest)
            self.mem_features.g3_total += b[2].g3_total
            self.mem_features.g3_longest = max(self.mem_features.g3_longest,b[2].g3_longest)
            self.mem_features.g4_total += b[2].g4_total
            self.mem_features.g4_longest = max(self.mem_features.g4_longest,b[2].g4_longest)
            self.mem_features.g5_total += b[2].g5_total
            self.mem_features.g5_longest = max(self.mem_features.g5_longest,b[2].g5_longest)
            self.mem_features.g6_total += b[2].g6_total
            self.mem_features.g6_longest = max(self.mem_features.g6_longest,b[2].g6_longest)

    def print_shadow_memory_features(self):
        print "DC total", self.mem_features.DC_total
        print "DC_longest", self.mem_features.DC_longest
        print "RI_total", self.mem_features.RI_total  
        print "OI_total", self.mem_features.OI_total 
        print "OI_longest", self.mem_features.OI_longest 
        print "g1_total", self.mem_features.g1_total 
        print "g1_longest", self.mem_features.g1_longest 
        print "g2_total", self.mem_features.g2_total 
        print "g2_longest", self.mem_features.g2_longest 
        print "g3_total", self.mem_features.g3_total 
        print "g3_longest", self.mem_features.g3_longest 
        print "g4_total", self.mem_features.g4_total 
        print "g4_longest", self.mem_features.g4_longest 
        print "g5_total", self.mem_features.g5_total 
        print "g5_longest", self.mem_features.g5_longest 
        print "g6_total", self.mem_features.g6_total 
        print "g6_longest", self.mem_features.g6_longest 

 
    def process_new_payload(self, addr, size):
        print "==== addr: ", hex(addr), "  size: ", size

        if len(self.blocks) == 0:
            features = self.scan(addr, addr+size-1)
            self.blocks.add((addr, addr+size-1, features))
        else:
            # p_block, f_block are not overlap with payload
            p_block_num = self.find_preceding_block(addr)
            print "p_block_num:", p_block_num
            f_block_num = self.find_following_block(addr+size)
            print "f_block_num:", f_block_num

            # Then, influenced blocks by the paylaod is p_block+1 ~ f_block-1
            i_start = p_block_num + 1
            i_end = f_block_num-1

            if i_end - i_start >= 0:        # if there is overlapped blocks
                low_addr = min(self.blocks[i_start][0], addr)
                high_addr = max(self.blocks[i_end][1], addr+size-1)
                for i in range(i_start,i_end+1):
                    print "(pop) ", hex(self.blocks[i_start][0])
                    self.blocks.pop(i_start)
                
                print "(add1) low addr: ", hex(low_addr), " high addr: ", hex(high_addr)
                features = self.scan(addr, addr+size-1)
                self.blocks.add((low_addr, high_addr, features))
            else:       
                print "(add2) low addr: ", hex(addr), " high addr: ", hex(addr+size-1)
                features = self.scan(addr, addr+size-1)
                self.blocks.add((addr, addr+size-1, features))

            print "# of blocks: ", len(self.blocks)

            for b in self.blocks:
                print hex(b[0]), ", ", hex(b[1])
            
            

    def find_preceding_block(self, target_addr):
        start = 0
        end = len(self.blocks)-1
        
        while end-start > 1:
            i = (start+end)/2
            if (self.blocks[i][1]+1 < target_addr):
                start = i
            else:
                end = i

        if target_addr <= self.blocks[start][1]+1:
            # No procedding block
            return -1
        elif target_addr > self.blocks[end][1]+1:
            # All blocks are proceding (no overlap)
            return len(self.blocks)-1
        else:
            # procedding block: blocks 0 ~ start
            return start

    def find_following_block(self, target_addr):
        start = 0
        end = len(self.blocks)-1
        
        while end-start > 1:
            i = (start+end)/2
            if (self.blocks[i][0]-1 > target_addr):
                end = i
            else:
                start = i

        if target_addr < self.blocks[start][0]-1:
            # All blocks are following
            return 0
        elif target_addr >= self.blocks[end][0]-1:
            # No following blocks
            return len(self.blocks)
        else:
            return end
        
         

def main():
    parser = argparse.ArgumentParser(description="Shadow Memory Map of M221 PLC")
    parser.add_argument("pcapFile", help="pcap file")

    args = parser.parse_args()

    shadow_mem = M221_shadow_memory()
    shadow_mem.process_pkts(args.pcapFile)
    shadow_mem.update_shadow_memory_features()
    shadow_mem.print_shadow_memory_features()
    #shadow_mem.print_mem()

if __name__ == '__main__':
    main()
