import os
from scapy.all import *
import argparse
from os import listdir
from os.path import isfile
from enip_tcp import *
from cip import *

# minimum size of control logic which has at least 1 input or 1 output
MIN_SIZE_LOGIC = 16  # rung start 2(0000), rung signature 2, rung size 2, input/output instruction 8, end instruction 2 

def get_pccc_pkts_info(pcapFile):
    all_pkts = rdpcap(pcapFile)

    pktNum = 0

    pccc_pkts_num = 0
    write_req_num = 0
    code_pkt_num = 0

    for pkt in all_pkts:
        pktNum += 1
        isPCCC = False

        # case 1) pccc is directly encapsulated in ENIP
        if ENIP_SendRRData in pkt and pkt[ENIP_SendRRData].items[1].type_id == 0x91:
            pccc = bytes(pkt[ENIP_SendRRData].items[1].payload)
            isPCCC = True
            #print binascii.hexlify(pccc)   
        elif CIP in pkt:
            try:
                cip_path = pkt[CIP][CIP_Path].path[1]
                if cip_path == '\x67':      # for PCCC?
                    cip_payload = bytes(pkt[CIP].payload)
                    if len(cip_payload) > 7:
                        pccc = cip_payload[7:]
                        isPCCC= True
            except:
                pass

        if isPCCC:
            pccc_pkts_num += 1
            # pccc[0] == 0x0f (request command) && pccc[4] == 0xaa (write)
            if pccc[0] == '\x0f' and pccc[4] == '\xaa':
                write_req_num += 1

                size = struct.unpack("B", pccc[5])[0]
                file_num = pccc[6]
                file_type = pccc[7]
                element_num = struct.unpack("B", pccc[8])[0]
                if pccc[9] == '\xff':       # the size of sub-element field is 3 bytes
                    subelement_num = struct.unpack("<H", pccc[10:12])[0]    
                    payload = pccc[12:]
                else:
                    subelement_num = struct.unpack("B", pccc[9])[0]
                    payload = pccc[10:]

                # To filter some dummy logic file which only contains END instruction (which have only 8 byte size)
                if file_type == '\x22' and not (subelement_num == 0 and size < MIN_SIZE_LOGIC):
                    code_pkt_num += 1

    return pccc_pkts_num, write_req_num, code_pkt_num

def main():
    parser = argparse.ArgumentParser(description="Get basic information from the PCCC pcap files")
    parser.add_argument("pcap_directory", help="directory containing pcap files")

    args = parser.parse_args()

    pcap_files = [f for f in listdir(args.pcap_directory) if isfile(os.path.join(args.pcap_directory,f)) and "pcap" in f.split(".")[-1]]
    

    total_pcaps = 0
    total_pccc_pkts = 0
    total_write_req = 0
    total_code_pkts = 0

    for f in pcap_files:
        total_pcaps += 1

        filePath = os.path.join(args.pcap_directory,f)
        print "File: " + filePath    

        pccc_pkts_num, write_req_num, code_pkt_num = get_pccc_pkts_info(filePath)
        total_pccc_pkts += pccc_pkts_num
        total_write_req += write_req_num
        total_code_pkts += code_pkt_num

        print total_pccc_pkts, total_write_req, total_code_pkts

    print "\nTotal # of pcap files: ", len(pcap_files)
    print "Total # of PCCC packets: ", total_pccc_pkts
    print "Total # of write request: ", total_write_req
    print "Total # of packet containg code: ", total_code_pkts

if __name__ == '__main__':
    main()
