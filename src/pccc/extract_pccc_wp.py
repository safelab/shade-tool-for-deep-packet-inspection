import argparse

from enip_tcp import *
from cip import *
from scapy.all import *

# minimum size of control logic which has at least 1 input or 1 output
MIN_SIZE_LOGIC = 16  # rung start 2(0000), rung signature 2, rung size 2, input/output instruction 8, end instruction 2 

# write payload: pkt number, address, size, (write) payload, isCode (is it code block?)
class wp():
    def __init__(self, pktNum, file_num, file_type, element_num, subelement_num, size, payload, isCode):
        self.pktNum = pktNum    
        self.file_num = file_num                # type: bytes
        self.file_type = file_type              # type: bytes
        self.element_num = element_num          # type: int
        self.subelement_num = subelement_num    # type: int 
        self.size = size
        self.payload = payload                  # type: bytes
        self.isCode = isCode

"""
    extract_pccc_wp
    - description: extract write payload of pccc protocol
    - input: pcap file
    - output: a list of wp objects
"""
def extract_pccc_wp(pcapFile):
    all_pkts = rdpcap(pcapFile)

    pktNum = 0

    wp_list = []

    for pkt in all_pkts:
        pktNum += 1
        isPCCC = False

        # case 1) pccc is directly encapsulated in ENIP
        if ENIP_SendRRData in pkt and pkt[TCP].dport == 44818 and pkt[ENIP_SendRRData].items[1].type_id == 0x91:
            pccc = bytes(pkt[ENIP_SendRRData].items[1].payload)
            isPCCC = True
            #print binascii.hexlify(pccc)   
        elif CIP in pkt and pkt[TCP].dport == 44818:
            cip_path = pkt[CIP][CIP_Path].path[1]
            if cip_path == '\x67':      # for PCCC?
                cip_payload = bytes(pkt[CIP].payload)
                if len(cip_payload) > 7:
                    pccc = cip_payload[7:]
                    isPCCC= True
        """
        elif CIP in pkt and pkt[TCP].dport == 44818:
            cip_payload = bytes(pkt[CIP].payload)
            if len(cip_payload) > 7:
                pccc = cip_payload[7:]
                isPCCC= True
                #print binascii.hexlify(pccc)
        """

        # pccc[0] == 0x0f (request command) && pccc[4] == 0xaa (write)
        if isPCCC and pccc[0] == '\x0f' and pccc[4] == '\xaa':
            size = struct.unpack("B", pccc[5])[0]
            file_num = pccc[6]
            file_type = pccc[7]
            element_num = struct.unpack("B", pccc[8])[0]
            if pccc[9] == '\xff':       # the size of sub-element field is 3 bytes
                subelement_num = struct.unpack("<H", pccc[10:12])[0]    
                payload = pccc[12:]
            else:
                subelement_num = struct.unpack("B", pccc[9])[0]
                payload = pccc[10:]

            # To filter some dummy logic file which only contains END instruction (which have only 8 byte size)
            if file_type == '\x22' and not (subelement_num == 0 and size < MIN_SIZE_LOGIC):
                isCode = True
            else:
                isCode = False

            wp_list.append(wp(pktNum, file_num, file_type, element_num, subelement_num, size, payload, isCode))
            
    return wp_list
            

def main():
    parser = argparse.ArgumentParser(description="Extract write packet payload of PCCC protocol")
    parser.add_argument("pcapFile", help="pcap file")

    args = parser.parse_args()

    wp_list = extract_pccc_wp(args.pcapFile)

    for wp in wp_list:
        print "* pkt [", wp.pktNum, "]"
        print "* file num: " + binascii.hexlify(wp.file_num)
        print "* file type: " + binascii.hexlify(wp.file_type)
        print "* size: ", wp.size
        print "* payload: " + binascii.hexlify(wp.payload)
        print "* code block: ", wp.isCode

if __name__ == '__main__':
    main()
