import os, sys
import argparse
import numpy as np
import argparse

def main():
    
    parser = argparse.ArgumentParser(description="Calculate detection rate of attack (code) instance")
    parser.add_argument("predict_file", help="predict file")

    args = parser.parse_args()

    pFile = open(args.predict_file, "r")

    lines = pFile.readlines()
    data_all = []

    # skip the first row (which is the names of the features)
    for line in lines[1:]:
        # the last chracter in each line is new line feed (\n)
        data_all.append(line[:-1].split(","))

    data_all = np.array(data_all)

    totalItems = data_all.shape[0]

    fileNames = data_all[:,0]
    target = data_all[:,-2]
    pred = data_all[:,-1]

    fp = 0
    tp = 0
    fn = 0
    tn = 0

    detected_files = []

    for i in range(totalItems):
        if target[i] == "0" and pred[i] == "1": # false positive
            #print "false positive"
            fp += 1

        elif target[i] == "1" and pred[i] == "1": # true positive
            tp += 1

            if fileNames[i] not in detected_files:
                detected_files.append(fileNames[i])
    
        elif target[i] == "1" and pred[i] == "0": # false negative
            fn += 1

        elif target[i] == "0" and pred[i] == "0": # true negative
            tn += 1

    fpr = fp / float(fp+tn)
    tpr = tp / float(tp+fn)

    """
    for fileName in fileNames:
        if fileName not in detected_files:
            print fileName
    """

    print "fpr: ", fpr
    print "tpr: %f (%d/%d)" % (tpr, tp, tp+fn) 
    print "# of detected pcap files (attack instances): ", len(detected_files)


if __name__ == '__main__':
    main()
