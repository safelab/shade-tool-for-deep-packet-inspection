import os, sys
import argparse
import numpy as np
from sklearn import datasets
from scipy import interp
from sklearn.naive_bayes import GaussianNB
#from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_val_predict

fpr_limit = 0.01

def get_data_target(lines):
    data_all = []

    # skip the first row (which is the names of the features)
    for line in lines[1:]:
        # the last chracter in each line is new line feed (\n)
        data_all.append(line[:-1].split(","))

    col_names = lines[0][:-1].split(",")

    data_all = np.array(data_all)

    # data with selected features
    # 1st col: file name, 2nd col: packet number, last col: class
    data = data_all[:,2:-1].astype(np.float)
    target = data_all[:,-1]

    return data, target, col_names

def get_evaluation_info(pred, target):
    numOfElement = len(pred)
    
    numOfError = (pred != target).sum()
    avg_err_rate = (numOfError / float(numOfElement)) * 100

    fp = 0
    tp = 0
    fn = 0
    tn = 0
    for i in range(len(pred)):
        if pred[i] == "1" and target[i] == "0":
            fp += 1
        elif pred[i] == "1" and target[i] == "1":
            tp += 1
        elif pred[i] == "0" and target[i] == "1":
            fn += 1
        elif pred[i] == "0" and target[i] == "0":
            tn += 1
    tpr = (tp / float(tp+fn)) * 100
    fnr = (fn / float(tp+fn)) * 100

    tnr = (tn / float(tn+fp)) * 100
    fpr = (fp / float(tn+fp)) * 100

    return avg_err_rate, fpr, tpr, fnr, tnr

def get_tpr_at_fpr_limit(clf, data, target, fpr_limit):
        fpr_range = np.linspace(0, fpr_limit*2, 100)

        pred_proba = clf.predict_proba(data)
        fpr, tpr, threshold = roc_curve(target, pred_proba[:,1], pos_label="1")
  
        tpr_range = []
        #threshold_range = []          
        tpr_range.append(interp(fpr_range, fpr, tpr))
        #threshold_range.append(interp(fpr_range, fpr, threshold))

        return tpr_range[0][50]


def main():
    parser = argparse.ArgumentParser(description="Feature test using (one-dimiensional) Guaussian Naive Bayes")
    parser.add_argument("-t", "--training_file", help="training file", required=True)
    parser.add_argument("-e", "--testing_file", help="testing file (Use the model generated basded on the training file)")

    
    args = parser.parse_args()

    print "* Training file: " + args.training_file
    trainingFile = open(args.training_file, "r")
    tr_outFile_name = args.training_file.split(".")[0]+".gnb_feature_test"
    tr_outFile = open(tr_outFile_name, "w")
    print "* Output file (training): " + tr_outFile_name
    tr_outFile.write("TPR %% at FPR %.3f %%,," % (fpr_limit * 100))

    tr_lines = trainingFile.readlines()
    tr_data, tr_target, tr_col_names = get_data_target(tr_lines)

    if args.testing_file:
        print "* Testing file: " + args.testing_file
        testingFile = open(args.testing_file, "r")
        test_outFile_name = args.testing_file.split(".")[0]+".gnb_feature_test"
        test_outFile = open(test_outFile_name, "w")
        print "* Output file (testing): " + test_outFile_name
        test_outFile.write("TPR %% at FPR %.3f %%,," % (fpr_limit * 100))

        test_lines = testingFile.readlines()
        test_data, test_target, test_col_names = get_data_target(test_lines)
        

    gnb = GaussianNB()
    
    k = 10

    print "* %d-fold CV result for each feature" % k
    print "------------------------------------"    
    
    for i in range(tr_data.shape[1]):
        tr_oneFeatureData = tr_data[:,i].reshape(-1, 1)
        gnb.fit(tr_oneFeatureData, tr_target)

        k = 10      # 10-fold validation
        
        pred = cross_val_predict(gnb, tr_oneFeatureData, tr_target, cv=k)
        avg_err_rate, fpr, tpr, fnr, tnr = get_evaluation_info(pred, tr_target)

        # Find TPR at FPR limit
        my_tpr = get_tpr_at_fpr_limit(gnb, tr_oneFeatureData, tr_target, fpr_limit)

        print "[%d] %s: %.2f %% (FPR: %f, TPR: %f, FNR: %f, TNR: %f)" % (i+2, tr_col_names[i+2], avg_err_rate, fpr, tpr, fnr, tnr)
        print "%.4f %% detection rate at %.4f %% FPR\n" % (my_tpr*100, fpr_limit*100)
        #tr_outFile.write("%.2f," % avg_err_rate)
        tr_outFile.write("%.3f," % (my_tpr*100))

        if args.testing_file:
            test_oneFeatureData = test_data[:,i].reshape(-1,1)

            pred = gnb.predict(test_oneFeatureData)
            avg_err_rate, fpr, tpr, fnr, tnr = get_evaluation_info(pred, test_target)

            my_tpr = get_tpr_at_fpr_limit(gnb, test_oneFeatureData, test_target, fpr_limit)

            print "(Test) [%d] %s: %.2f %% (FPR: %f, TPR: %f, FNR: %f, TNR: %f)" % (i+2, tr_col_names[i+2], avg_err_rate, fpr, tpr, fnr, tnr)
            print "(Test) %.4f %% detection rate at %.4f %% FPR\n" % (my_tpr*100, fpr_limit*100)
            test_outFile.write("%.3f," % (my_tpr*100))

            #pred_proba2 = gnb.predict_proba(test_oneFeatureData)
            #pred_mine = np.where(pred_proba2[:,1] > my_threshold, "1", "0")

    
    tr_outFile.write("\n")
    for line in tr_lines:
        tr_outFile.write(line)

    test_outFile.write("\n")
    for line in test_lines:
        test_outFile.write(line)

    trainingFile.close()
    tr_outFile.close()
    test_outFile.close()

if __name__ == '__main__':
    main()
