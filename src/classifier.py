import os, sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy import interp
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_predict
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
import pickle

# feature column
selected_feature = [12]   
#selected_feature = [3,27]   # #dec, L4gram for M221
#selected_feature = [3,12]   # #dec, #8gram for ML1400 (pccc)
roc_fpr_max = 0.01

def get_threshold_at_fpr_limit(clf, data, target, fpr_limit):
        fpr_range = np.linspace(0, fpr_limit*2, 100)

        pred_proba = clf.predict_proba(data)
        fpr, tpr, threshold = roc_curve(target, pred_proba[:,1], pos_label="1")
  
        #tpr_range = []
        #tpr_range.append(interp(fpr_range, fpr, tpr))
        threshold_range = interp(fpr_range, fpr, threshold)

        return threshold_range[50]

def main():
    parser = argparse.ArgumentParser(description="Data classification with Gaussian Naive Bayes")
    group1 = parser.add_mutually_exclusive_group(required=True)
    group1.add_argument("-b", "--gnb", help="Guassian Naive Bayes Classifier", action="store_true")
    group1.add_argument("-s", "--svm", help="Support Vector Machine Classifier", action="store_true")

    group2 = parser.add_mutually_exclusive_group(required=True)
    group2.add_argument("-t", "--training_file", help="training data file (we perfome 10-fold cross validation)")
    group2.add_argument("-e", "--testing_file", help="testing data file")
    parser.add_argument("-m", "--model_file", required='-e' in sys.argv, help="model file name")
    parser.add_argument("-f", "--fpr", help="predict at the desired false positive rate")
    #parser.add_argument("out_file", help="output file name with prediction")
    
    args = parser.parse_args()

    features_str = "+".join(map(str, selected_feature))

    if args.training_file:
        print "* Training data file: " + args.training_file
        dataFile = open(args.training_file, "r")
        if args.gnb:
            outFileName = args.training_file.split(".")[0] + "_" + features_str + "_" + str(args.fpr)  + ".gnb_predicted"
            model_file_name = args.training_file.split(".")[0] + "_" + features_str + ".gnb_model"
            roc_file_name = args.training_file.split(".")[0] + "_" + features_str + ".gnb_roc"
        else:
            outFileName = args.training_file.split(".")[0] + "_" + features_str + "_" + str(args.fpr) +".svm_predicted"
            model_file_name = args.training_file.split(".")[0] + "_" + features_str + ".svm_model"
            roc_file_name = args.training_file.split(".")[0] + "_" + features_str + ".svm_roc"
        print "* Output model file name: " + model_file_name
    else:
        print "* Testing data file: " + args.testing_file
        print "* Loaded Model file: " + args.model_file
        dataFile = open(args.testing_file, "r")
        if args.gnb:
            outFileName = args.testing_file.split(".")[0] + "_" + features_str + "_" + str(args.fpr) + ".gnb_predicted"
            roc_file_name = args.testing_file.split(".")[0] + "_" + features_str + ".gnb_roc"
        else:   
            outFileName = args.testing_file.split(".")[0] + "_" + features_str + "_" + str(args.fpr) + ".svm_predicted"
            roc_file_name = args.testing_file.split(".")[0] + "_" + features_str + ".svm_roc"


    print "* Output file with predicted label: " + outFileName
    outFile = open(outFileName, "w")

    print "* ROC curve file name: " + roc_file_name
    rocFile = open(roc_file_name, "w")    


    lines = dataFile.readlines()
    data_all = []

    # skip the first row (which is the names of the features)
    for line in lines[1:]:
        # the last chracter in each line is new line feed (\n)
        data_all.append(line[:-1].split(","))

    col_names = lines[0][:-1].split(",")

    data_all = np.array(data_all)

    # data with selected features
    # 1st col: file name, 2nd col: packet number, last col: class
    #data = data_all[:,2:-1].astype(np.float)

    selected_feature_names = [col_names[i] for i in selected_feature]
    print "* Selected feature: ", selected_feature_names
    

    data = data_all[:,selected_feature].astype(np.float)
    target = data_all[:,-1]

    if args.training_file:
        if args.gnb:
            print "* Classifier: Gaussian Naive Bayes"
            clf = GaussianNB()  # GNB classifier
        else:
            print "* Classifier: SVM Classifier"
            clf = svm.SVC(kernel='rbf', probability=True)     # SVM classifier      # probability = Ture : to make ROC curve

        # make model and save it
        clf.fit(data, target)
        pickle.dump(clf, open(model_file_name, 'wb'))

        # cross-validation (10-fold)
        k = 10
        pred = cross_val_predict(clf, data, target, cv=k)
        numOfError = (pred != target).sum()
        avg_err_rate = (numOfError / float(data.shape[0])) * 100
        print "* Error (FP+FN) rate from %d-fold CV: %f %%\n" % (k, avg_err_rate)

        # To draw ROC curve
        tprs = []
        aucs = []
        #mean_fpr = np.linspace(0, 1, 100)
        mean_fpr = np.linspace(0, roc_fpr_max, 11)

        cv = StratifiedKFold(n_splits=k)

        for train, test in cv.split(data, target):
            probas_ = clf.fit(data[train], target[train]).predict_proba(data[test])
        
            # Compute ROC curve and area under the curve (AUC)
            fpr, tpr, thresholds = roc_curve(target[test], probas_[:, 1], pos_label="1")
            tprs.append(interp(mean_fpr, fpr, tpr))
            tprs[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            aucs.append(roc_auc)

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        
        print "* ROC curve *"
        print "mean_tpr: ", mean_tpr
        print "mean_fpr: ", mean_fpr
        print "mean_auc: ", mean_auc
        print "std_auc: ", std_auc  

        rocFile.write(",".join(mean_tpr.astype('|S6')))      
        rocFile.write("\n")
        rocFile.write(",".join(mean_fpr.astype('|S6')))      

    else:
        try:
            clf = pickle.load(open(args.model_file, 'rb'))
        except:
            print "Error. Can't open the model file: " + args.model_file
            sys.exit(1)
 
        pred = clf.predict(data)
        numOfError = (target != pred).sum()
        
        avg_err_rate = (numOfError / float(data.shape[0])) * 100
        print "* Error (FP+FN) rate: %f %%\n" % (avg_err_rate)

        probas_ = clf.predict_proba(data)
        fpr, tpr, thresholds = roc_curve(target, probas_[:, 1], pos_label="1")

        fpr_space = np.linspace(0, roc_fpr_max, 11)
        tpr_space = interp(fpr_space, fpr, tpr)
        auc_value = auc(fpr_space, tpr_space)

#        print "true_fpr: ", fpr
#        print "true_tpr: ", tpr
        
        """
        print "ture fpr"
        for f in fpr:
            print "%.4f" % f
        print "ture tpr"
        for t in tpr:
            print "%.4f" % f
        print "thresholds"
        for thr in thresholds:
            print "%.4f" % thr
        """

        print "* ROC curve *"
        print "tpr: ", tpr_space
        print "fpr: ", fpr_space
        print "auc: ", auc_value

        rocFile.write(",".join(tpr_space.astype('|S6')))      
        rocFile.write("\n")
        rocFile.write(",".join(fpr_space.astype('|S6')))      

    outFile.write("%s,%s," % (col_names[0], col_names[1]))  # file name, packet number

    for name in selected_feature_names:
        outFile.write("%s," % name)

    outFile.write("class, pred\n")


    # Write prediction result based on the desired false positive rate
    if args.fpr is not None:
        my_threshold = get_threshold_at_fpr_limit(clf, data, target, float(args.fpr))
        proba2 = clf.predict_proba(data)
        pred = np.where(proba2[:,1] > my_threshold, "1", "0")

#        pred = np.where(data > 3, "1", "0")


    for i in range(data.shape[0]):        # row
        outFile.write("%s,%s," % (data_all[i,0], data_all[i,1]))  # file name, packet number
        outFile.write(",".join(data[i,:].astype('|S3')))
        outFile.write(",%s,%s\n" % (target[i], pred[i][0]))

    dataFile.close()
    outFile.close()
    rocFile.close()

if __name__ == '__main__':
    main()
